<html>

<head>
	<title>Form đăng ký thành viên</title>
	<link rel="stylesheet" href="./resource/css/style2.css">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/def9fb5410.js"></script>
	<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"
		type="text/javascript"></script>
	<script src="./resource/js/script2.js"></script>
</head>

<body onload="checkCookie()">
	<?php
		$name=$email=$username=$password=$idcard=$numphone=$birthday=$gender=$addr=$note="";
		require_once("connection2.php");
		if (isset($_POST["btn_submit"])) {
              //lấy thông tin từ các form bằng phương thức POST
            $name = $_POST["name"];
  			$email = $_POST["email"];
  			$username = $_POST["username"];
			$password = $_POST["password"];
			$mssv = $_POST["idcard"];
			$numphone = $_POST["numphone"];
			$birthday = $_POST["birthday"];
			$gender = $_POST["gender"];
			$addr = $_POST["addr"];
			$note = $_POST["note"];
  			//Kiểm tra điều kiện bắt buộc đối với các field không được bỏ trống
			  if ($username == "" || $password == "" || $name == "" || $email == "" || $mssv == "" || $numphone = "") {
				$message="Bạn cần nhập đủ thông tin";
                echo "<label class='err'>$message</label>";
  			}else{
  					// Kiểm tra tài khoản đã tồn tại chưa
  					$sql="select * from users where username='$username'";
					$kt=mysqli_query($conn, $sql);
 
					if(mysqli_num_rows($kt)  > 0){
						$message="Tài khoản đã tồn tại";
						echo "<label class='err'>$message</label>";
					}else{
						//thực hiện việc lưu trữ dữ liệu vào db
	    				$sql = "INSERT INTO users(
	    					username,
	    					password,
	    					name,
						    email,
							mssv,
							numphone,
							birthday,
							sex,
							addr,
							note
	    					) VALUES (
	    					'$username',
	    					'$password',
						    '$name',
	    					'$email',
							'$mssv',
							'$numphone',
							'$birthday',
							'$gender',
							'$addr',
							'$note'

	    					)";
					    // thực thi câu $sql với biến conn lấy từ file connection.php
						mysqli_query($conn,$sql);
						echo ("<script language='javascript'>
							window.alert('Đăng kí thành công')
							window.location.href = 'login.php';
						</script>");
						
					}
									    			
			  }
	}
	?>
	<form action="register.php" id="demoForm" method="POST" accept-charset="utf-8">
		<h1>Register</h1>
		<div class="textbox">
			<input type="text" placeholder="Full name" name="name" id="name" value="<?php echo $name;?>">
		</div>
		<div class="textbox required">
			<input type="text" placeholder="Username" name="username" id="username" value="<?php echo $username;?>">
		</div>
		<div class="textbox required">
			<input type="email" placeholder="Your email" name="email" id="email" value="<?php echo $email;?>">
		</div>
		<div class="textbox required">
			<input type="text" placeholder="Your ID student card" name="idcard" id="idcard"
				value="<?php echo $idcard;?>">
		</div>
		<div class="textbox required">
			<input type="text" placeholder="Your numberphone" name="numphone" id="numphone"
				value="<?php echo $numphone;?>">
		</div>
		<div class="textbox required">
			<input type="password" placeholder="Your password" name="password" id="password"
				value="<?php echo $password;?>">
		</div>
		<div class="textbox required">
			<input type="password" placeholder="Confirm password" name="confirm_password" id="confirm_password">
		</div>
		<div class="textbox">
			<input type="date" placeholder="Birthday" name="birthday" id="birtday" value="<?php echo $birthday;?>">
		</div>
		<div class="textbox">
			<input type="text" placeholder="Gender" name="gender" id="gender" value="<?php echo $gender;?>">
		</div>
		<div class="textbox">
			<input type="text" placeholder="Your address" name="addr" id="addr" value="<?php echo $addr;?>">
		</div>
		<div class="textbox">
			<input type="text" placeholder="Note" name="note" id="note" value="<?php echo $note;?>">
		</div>
		<input type="submit" class="btn" name="btn_submit" value="Register">
		<div class="cookie">
			<button onclick="checkCookie()">Click to create cookie</button>
			<button onclick="deleteCookie()">Click to delete cookie</button>
		</div>
	</form>
</body>

</html>