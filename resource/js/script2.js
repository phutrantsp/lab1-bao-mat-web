(function($) {
	$.fn.inputFilter = function(inputFilter) {
	  return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
		if (inputFilter(this.value)) {
		  this.oldValue = this.value;
		  this.oldSelectionStart = this.selectionStart;
		  this.oldSelectionEnd = this.selectionEnd;
		} else if (this.hasOwnProperty("oldValue")) {
		  this.value = this.oldValue;
		  this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
		}
	  });
	};
  }(jQuery));
$(document).ready(function() {
	$("#demoForm").validate({
		onfocusout: false,
		onkeyup: false,
		onclick: false,
		rules: {
			"username": {
				required: true,
				maxlength: 15
			},
			"password": {
				required: true,
				minlength: 8
			},
			"confirm_password": {
				equalTo: "#password",
				minlength: 8
				
			},
			"email": {
				required: true,
				email: true
			},		
			"numphone": {
				required: true,
				maxlength: 15
			},
			"idcard": {
				required: true,
				maxlength: 15
			}
		},
		messages: {
			"username": {
				required: "Bắt buộc nhập username",
				maxlength: "Hãy nhập tối đa 15 ký tự"
			},
			"password": {
				required: "Bắt buộc nhập password",
				minlength: "Hãy nhập ít nhất 8 ký tự"
			},
			"confirm_password": {
				equalTo: "Hai password phải giống nhau",
				minlength: "Hãy nhập ít nhất 8 ký tự"
			},
			"email":{
				required : "Email không được để trống",
				email : "Email không đúng định dạng",
			},
			"numphone": {
				required: "Numberphone không được để trống",
				maxlength: "Hãy nhập tối đa 15 ký tự"
			},
			"idcard": {
				required: "MSSV không được để trống",
				maxlength: "Hãy nhập tối đa 15 ký tự"
			}
		}
	});
	$("#name").inputFilter(function(value) {
		return /^[a-zA-Z ]*$/.test(value)
	  });
});
function generate_random_string(string_length){
    let random_string = '';	
    let random_ascii;
    for(let i = 0; i < string_length; i++) {
        random_ascii = Math.floor((Math.random() * 25) + 97);
        random_string += String.fromCharCode(random_ascii)
    }
    return random_string
}
function checkCookie()
        {
		  var user = accessCookie("testCookie");
		  //alert("user:" + user);
          if (user!="")
        	alert("Welcome Back!");
          else
          {
            createCookie("testCookie");
          }
		}
function createCookie(cookieName)
        {
          var date = new Date();
          date.setTime(date.getTime()+(3*24*60*60*1000)); //2days Expire
          document.cookie = cookieName + "=" + generate_random_string(20) + "; expires=" + date.toGMTString() + ";path=/";
		  alert("Created cookie!!");
		}
function accessCookie(cookieName)	
        {
			//alert("Access cookie!!");
			var name = cookieName + "=";
			var decodedCookie = decodeURIComponent(document.cookie);
			//alert("de"+decodedCookie.toString());
			var ca = decodedCookie.split(';');
			for(var i = 0; i < ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == ' ') {
				c = c.substring(1);
				}
				if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
				}
			}
			return "";
        	
		}

function deleteCookie()
{	
	var date = new Date()
	date.setTime(date.getTime() + (24*60*60*1000) ); 
	document.cookie = "testCookie=; expires=" + date.toGMTString() + ";path=/";
	alert("Deleted cookie!!");
}