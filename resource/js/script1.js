(function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
  };
}(jQuery));
$(document).ready(function () {
    $(".textbox input").focus(function(){
        $(this).addClass("focus");
      });
    
    $(".textbox input").blur(function(){
        if($(this).val() == "")
        $(this).removeClass("focus");
      });
    $("#username").inputFilter(function(value) {
      return /^[a-zA-Z ]*$/.test(value)
    });
  });
function change(){
  var obj=document.getElementById('css1');
  obj.outerHTML='<link id="css1" rel="stylesheet" href="./resource/css/style2.css"></link>'
}