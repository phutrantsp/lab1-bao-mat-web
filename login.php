
<html>

<head>
    <title>Trang đăng nhập</title>
    <meta charset="utf-8">
    <link id="css1" rel="stylesheet" href="./resource/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/def9fb5410.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"
        type="text/javascript"></script>
    <script src="./resource/js/script1.js"></script>
    <!--để sau jquery-->
</head>

<body>
    <form class="login" method="POST" action="validate.php">
        <h1>Login</h1>
        <div class="clearfix"></div>
        <div class="textbox">
            <input type="text" id="username" name="username">
            <span data-placeholder="Username"></span>
        </div>
        <div class="textbox">
            <input type="password" id="password" name="password" >
            <span data-placeholder="Password"></span>
        </div>
        <div class="remember">

            <input type="checkbox" name="remember" value="1">

            <label>Remember login</label>

        </div>

        <input type="submit" class="btn" value="Sign in" name="btn_submit">
        <div class="register">
            <a href="register.php">Register</a>
        </div>
        <script type="text/javascript">
        </script>
    </form>
</body>

</html>
<?php
require_once("connection.php");
if(empty($_SESSION['username'])){	      
    if(isset($_COOKIE[$cookie_name])){    
        parse_str($_COOKIE[$cookie_name]);
        $array_usr_pass=explode("&",$_COOKIE[$cookie_name]) ;
     
        $user=substr($array_usr_pass[0],strpos($array_usr_pass[0],"=")+1) ;  
        $pass=substr($array_usr_pass[1],strpos($array_usr_pass[1],"=")+1);
        echo "<script>
        document.getElementById('username').value='$user';
        document.getElementById('password').value='$pass';
        </script>";
        echo $user."<br/>".$pass;
    }  
    else echo "Không tồn tại cookie" ;

}
else{
     
    header('location:index.php');//chuyển qua trang đăng nhập thành công
     
    exit;
         
}  	   
?>