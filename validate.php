<?php
require_once("login.php");
function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
};
if (isset($_POST["btn_submit"])) 
{
    // lấy thông tin người dùng
    $username = test_input($_POST["username"]);
    $password = test_input($_POST["password"]);
    $a_check=((isset($_POST['remember'])!=0)?1:"");

    //làm sạch thông tin, xóa bỏ các tag html, ký tự đặc biệt 
    //mà người dùng cố tình thêm vào để tấn công theo phương thức sql injection	

    if ($username == "" || $password =="") 
    {
        $message="Tên đăng nhập và mật khẩu không được để trống";
        echo '<label class="err">'.$message.'</label>';
    }
    else
    {
        $sql = "select * from users where username = '$username' and password = '$password' ";
        $query = mysqli_query($conn,$sql);
        $num_rows = mysqli_num_rows($query);
        if ($num_rows==0) {
            
            $message="Tên đăng nhập hoặc mật khẩu không đúng";
            echo '<label class="err">'.$message.'</label>';    
        }else
        {
            //tiến hành lưu tên đăng nhập vào session để tiện xử lý sau này
           
            if($a_check==1){
                setcookie ($cookie_name, 'usr='.$username.'&pass='.$password, time() + $cookie_time);   	 
            }  
            session_start(); 
            $_SESSION['username'] = $username;
            $_SESSION['password']= $password;
                  
            // Thực thi hành động sau khi lưu thông tin vào session
            // ở đây mình tiến hành chuyển hướng trang web tới một trang gọi là index.php
            echo ("<script language='javascript'>
						window.alert('Đăng nhập thành công')
						window.location.href='login.php';
						</script>");
            header('Location: index.php');
            exit;
        }
    }
}
?>