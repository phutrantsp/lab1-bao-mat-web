<?php
session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title>
        Login
    </title>
    <link rel="stylesheet" href="./resource/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/def9fb5410.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"
        type="text/javascript"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
    <script src="./resource/js/script1.js"></script>
    <!--để sau jquery-->
</head>

<body>
    <div class="container">
        <div class="form-box">
            <div class="button-box">
                <div id="btn"></div>
                <button type="button" class="toggle-btn" id="btn_login">Login</button>
                <button type="button" class="toggle-btn" id="btn_register">Register</button>
            </div>
            <form action="" class="demoLogin" id="Login" action="validate.php" method="POST" >
                <input type="text" class="input-field"  id="username_login" name="username_login" placeholder="Username" required >
                <input type="password" class="input-field"  id="password_login" name="password_login" placeholder="Password" required>
                <div class="check-box">
                    <input type="checkbox" name="remember" id="remember" value="Yes"><span>Remember</span>
                </div>
                <input type="submit" class="submit-btn" name="login_submit" value="Login" onclick="postAjax(); return false;">
                <div class="message">
                 <ul id="form-message"></ul>
                </div>
            </form>
            <form action="register.php" class="demoLogin" id="Register" method="POST">
                
                <input type="text" placeholder="Full name" name="name" id="name" class="input-field ">
                
                
                <div class="textbox">
                <input type="text" placeholder="Username" name="username_register" id="username" class="input-field required" >
                <i class="i-required"></i>
                </div>
                <div class="textbox">
                <input type="email" placeholder="Your email" name="email" id="email" class="input-field required">
                <i class="i-required"></i>
                </div>
                <div class="textbox">
                <input type="text" placeholder="Your ID student card" name="idcard" id="idcard" class="input-field required">
                <i class="i-required"></i>
                </div>
                <div class="textbox">
                <input type="text" placeholder="Your numberphone" name="numphone" id="numphone" class="input-field required">
                <i class="i-required"></i>
                </div>
                <div class="textbox">
                <input type="password" placeholder="Your password" name="password_register" id="password_register" class="input-field required">
                <i class="i-required"></i>
                </div>
                <div class="textbox">
                <input type="password" placeholder="Confirm password" name="confirm_password" id="confirm_password"
                    class="input-field ">
                <i class="i-required"></i>
                </div>

                <input type="date" placeholder="Birthday" name="birthday" id="birtday" class="input-field">


                <input type="text" placeholder="Gender" name="gender" id="gender" class="input-field ">


                <input type="text" placeholder="Your address" name="addr" id="addr" class="input-field ">


                <input type="text" placeholder="Note" name="note" id="note" class="input-field">

                <input type="submit" class="submit-btn" value="Register" name="register_submit">
                <div class="cookie">
			        <button onclick="checkCookie()">Click to create cookie</button>
			        <button onclick="deleteCookie()">Click to delete cookie</button>
		        </div>

            </form>
        </div>
    </div>

</body>

</html>
<?php
require_once("connection.php");
if(empty($_SESSION['username'])){	      
    if(isset($_COOKIE[$cookie_name])){    
        parse_str($_COOKIE[$cookie_name]);
        $array_usr_pass=explode("&",$_COOKIE[$cookie_name]) ;
     
        $user=substr($array_usr_pass[0],strpos($array_usr_pass[0],"=")+1) ;  
        $pass=substr($array_usr_pass[1],strpos($array_usr_pass[1],"=")+1);
        echo "<script>
        document.getElementById('username_login').value='$user';
        document.getElementById('password_login').value='$pass';
        </script>";
        echo $user."<br/>".$pass;
    }  
    // else echo "Không tồn tại cookie" ;

}
else{
     
    header('location:index.php');//chuyển qua trang đăng nhập thành công
     
    exit;
         
}  	   
?>