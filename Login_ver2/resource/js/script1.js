(function($) {
	$.fn.inputFilter = function(inputFilter) {
	  return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
		if (inputFilter(this.value)) {
		  this.oldValue = this.value;
		  this.oldSelectionStart = this.selectionStart;
		  this.oldSelectionEnd = this.selectionEnd;
		} else if (this.hasOwnProperty("oldValue")) {
		  this.value = this.oldValue;
		  this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
		}
	  });
	};
  }(jQuery));
$(document).ready(function(){
    $("#btn_login").click(function(){
      $("#Login").css("left","5rem");
      $("#Register").css("left","45rem");
      $("#btn").css("left","0");
      $(".form-box").css("height","35rem");
    });
    $("#btn_register").click(function(){
      $("#Login").css("left","-40rem");
      $("#Register").css("left","5rem");
      $("#btn").css("left","11rem");
      $(".form-box").css("height","80rem");
    });
    $("#Register").validate({
      onfocusout: false,
      onkeyup: false,
      onclick: false,
      rules: {
        "username_register": {
          required: true,
          maxlength: 15
        },
        "password_register": {
          required: true,
          minlength: 8
        },
        "confirm_password": {
          equalTo: "#password_register",
          minlength: 8
          
        },
        "email": {
          required: true,
          email: true
        },		
        "numphone": {
          required: true,
          maxlength: 15
        },
        "idcard": {
          required: true,
          maxlength: 15
        }
      },
      messages: {
        "username_register": {
          required: "Bắt buộc nhập username",
          maxlength: "Hãy nhập tối đa 15 ký tự"
        },
        "password_register": {
          required: "Bắt buộc nhập password",
          minlength: "Hãy nhập ít nhất 8 ký tự"
        },
        "confirm_password": {
          equalTo: "Hai password phải giống nhau",
          minlength: "Hãy nhập ít nhất 8 ký tự"
        },
        "email":{
          required : "Email không được để trống",
          email : "Email không đúng định dạng",
        },
        "numphone": {
          required: "Numberphone không được để trống",
          maxlength: "Hãy nhập tối đa 15 ký tự"
        },
        "idcard": {
          required: "MSSV không được để trống",
          maxlength: "Hãy nhập tối đa 15 ký tự"
        }
      }
    });
  	$("#name").inputFilter(function(value) {
      return /^[a-zA-Z ]*$/.test(value)
      });
    $("#username_login").inputFilter(function(value) {
      return /^[a-zA-Z ]*$/.test(value)
      });


});
function generate_random_string(string_length){
  let random_string = '';	
  let random_ascii;
  for(let i = 0; i < string_length; i++) {
      random_ascii = Math.floor((Math.random() * 25) + 97);
      random_string += String.fromCharCode(random_ascii)
  }
  return random_string
}
function checkCookie()
      {
    var user = accessCookie("testCookie");
    //alert("user:" + user);
        if (user!="")
        alert("Welcome Back!");
        else
        {
          createCookie("testCookie");
        }
  }
function createCookie(cookieName)
      {
        var date = new Date();
        date.setTime(date.getTime()+(3*24*60*60*1000)); //2days Expire
        document.cookie = cookieName + "=" + generate_random_string(20) + "; expires=" + date.toGMTString() + ";path=/";
    alert("Created cookie!!");
  }
function accessCookie(cookieName)	
      {
    //alert("Access cookie!!");
    var name = cookieName + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    //alert("de"+decodedCookie.toString());
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
      c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
      }
    }
    return "";
        
  }

function deleteCookie()
{	
var date = new Date()
date.setTime(date.getTime() + (24*60*60*1000) ); 
document.cookie = "testCookie=; expires=" + date.toGMTString() + ";path=/";
alert("Deleted cookie!!");
}
function postAjax()
{
    const form = {
      messages: document.getElementById('form-message'),
      user:document.getElementById("username_login"),
      pass:document.getElementById("password_login"),
      rem:document.getElementById("remember")

    };
    
    
    const request=new XMLHttpRequest();
    request.onload = () => {
      let responseObject = null;
        try {
              responseObject = JSON.parse(request.responseText);
            } 
        catch (e) 
            {
              console.error('Could not parse JSON!');
            }
        if (responseObject) 
            {
                    handleResponse(responseObject,form);
            }
    };
    const requestData='username_login='+form.user.value+"&password_login="+form.pass.value+"&remember"+form.rem;
    request.open('post','validate.php',true);
    request.setRequestHeader('Content-type','application/x-www-form-urlencoded')
    request.send(requestData)
}
        
   
   
function handleResponse(responseObject,form)
{
    if (responseObject.ok) 
    {
        location.href="index.php";
    } 
    else 
    {
        while (form.messages.firstChild) 
        {
        form.messages.removeChild(form.messages.firstChild);
        }
            
        responseObject.messages.forEach((message) => {
        const li = document.createElement('li');
        li.textContent = message;
            form.messages.appendChild(li);
        });
    }
}